package com.zwh.app.ui;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tencent.weibo.api.StatusesAPI;
import com.tencent.weibo.api.UserAPI;
import com.tencent.weibo.constants.OAuthConstants;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.zwh.android_hao.R;
import com.zwh.app.bean.UserInfo;
import com.zwh.app.common.*;
import com.zwh.app.manager.AppContext;
import com.zwh.app.manager.AppException;
import com.zwh.app.widget.lazy.ImageLoader;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * 用户资料
 * @author liux (http://my.oschina.net/liux)
 * @version 1.0
 * @created 2012-3-21
 */
public class UserInfoActivity extends BaseActivity{	
	
	private ImageView back;
	private ImageView refresh;
	private ImageView face;
	private ImageView gender;
	private Button editer;
	private TextView name;
	private TextView jointime;
	private TextView from;
	private TextView devplatform;
	private TextView expertise;
	private TextView followers;
	private TextView fans;
	private TextView favorites;
	private LinearLayout favorites_ll;
	private LinearLayout followers_ll;
	private LinearLayout fans_ll;

	private Handler mHandler;
		
	private final static int CROP = 200;
	private final static String FILE_SAVEPATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/OSChina/Portrait/";
	private Uri origUri;
	private Uri cropUri;
	private File protraitFile;
	private Bitmap protraitBitmap;
	private String protraitPath;
	public ImageLoader imageLoader; 
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//设置全屏
		UIHelper.showFullScreen(this);
		setContentView(R.layout.user_info);
		AppContext haoDroid = ((AppContext)getApplicationContext());
        imageLoader=haoDroid.getImageLoader();
		
		//初始化视图控件
		this.initView();
		//初始化视图数据
		this.initData();
	}
	
	private void initView(){
		back = (ImageView)findViewById(R.id.user_info_back);
		refresh = (ImageView)findViewById(R.id.user_info_refresh);
//		editer = (Button)findViewById(R.id.user_info_editer);
		back.setOnClickListener(UIHelper.finish(this));
		refresh.setOnClickListener(refreshClickListener);
		
		
		face = (ImageView)findViewById(R.id.user_info_userface);
		gender = (ImageView)findViewById(R.id.user_info_gender);
		name = (TextView)findViewById(R.id.user_info_username);
		jointime = (TextView)findViewById(R.id.user_info_jointime);
		from = (TextView)findViewById(R.id.user_info_from);
		devplatform = (TextView)findViewById(R.id.user_info_devplatform);
		expertise = (TextView)findViewById(R.id.user_info_expertise);
		followers = (TextView)findViewById(R.id.user_info_followers);
		fans = (TextView)findViewById(R.id.user_info_fans);
		favorites = (TextView)findViewById(R.id.user_info_favorites);
		favorites_ll = (LinearLayout)findViewById(R.id.user_info_favorites_ll);
		followers_ll = (LinearLayout)findViewById(R.id.user_info_followers_ll);
		fans_ll = (LinearLayout)findViewById(R.id.user_info_fans_ll);
	}
	
	private void initData(){
		mHandler = new Handler(){
			public void handleMessage(Message msg) {
				
				if(msg.what == 1 && msg.obj != null){
					UserInfo user = (UserInfo)msg.obj;
					
					//加载用户头像
//					UIHelper.showUserFace(face,info.getHead());
					String url=user.getHead()+"/50";
					imageLoader.DisplayImage(url, face);
					
					//其他资料
					name.setText(user.getName());
					
					favorites.setText(user.getFavnum()+"");
					followers.setText(user.getIdolnum()+"");
					fans.setText(user.getFansnum()+"");
					
					String birthday=user.getBirth_year()+"-"+user.getBirth_month()+"-"+user.getBirth_day();
					jointime.setText(birthday);
					from.setText(user.getNick());
					devplatform.setText(user.getLevel());
					expertise.setText(user.getLocation());
					
				
				
					
				}else if(msg.obj != null){
//					((AppException)msg.obj).makeToast(UserInfo.this);
				}
			}
		};		
		this.loadUserInfoThread();
	}
	
	private void loadUserInfoThread(){
		/*loading = new LoadingDialog(this);		
		loading.show();*/
		
		new Thread(){
			public void run() {
				Message msg = new Message();
				try {
//					MyInformation user = ((AppContext)getApplication()).getMyInformation(isRefresh);
					
					OAuthV2 oAuthV2=null;
		        	AppContext haoDroid = ((AppContext)getApplicationContext());
		        	oAuthV2 =(OAuthV2) haoDroid.getOauth();
		        	UserAPI userAPI;
		        	userAPI=new UserAPI(OAuthConstants.OAUTH_VERSION_2_A);
		        	String json=userAPI.info(oAuthV2, "json");//调用QWeiboSDK获取用户信息

		        	UserInfo userinfo=UserInfo.getUser(json);
					msg.what = 1;
	                msg.obj = userinfo;
	            } catch (Exception e) {
	            	e.printStackTrace();
	            	msg.what = -1;
	                msg.obj = e;
	                return ;
	            }
				mHandler.sendMessage(msg);
			}
		}.start();
	}
	
	
	
	private View.OnClickListener refreshClickListener = new View.OnClickListener(){
		public void onClick(View v) {
			loadUserInfoThread();
		}
	};
	
	private View.OnClickListener favoritesClickListener = new View.OnClickListener(){
		public void onClick(View v) {
//			UIHelper.showUserFavorite(v.getContext());
		}
	};

	
			
	
		
	

}