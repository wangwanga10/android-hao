package com.zwh.app.ui;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;




import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;

import com.tencent.weibo.api.PrivateAPI;
import com.tencent.weibo.api.StatusesAPI;
import com.tencent.weibo.constants.OAuthConstants;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.tencent.weibo.oauthv2.OAuthV2Client;
import com.tencent.weibo.webview.OAuthV2AuthorizeWebView;
import com.zwh.android_hao.R;
import com.zwh.app.adapter.ListViewHomeLineAdapter;
import com.zwh.app.adapter.ListViewMentionsTimelineAdapter;
import com.zwh.app.adapter.ListViewPrivateAdapter;
import com.zwh.app.bean.MentionPaser;
import com.zwh.app.bean.PrivatePaser;
import com.zwh.app.bean.StatusPaser;
import com.zwh.app.common.StringUtils;
import com.zwh.app.common.UIHelper;
import com.zwh.app.manager.AppConfig;
import com.zwh.app.manager.AppContext;
import com.zwh.app.manager.AppManager;
import com.zwh.app.widget.PullToRefreshListView;
import com.zwh.app.widget.ScrollLayout;
import com.zwh.app.widget.greendroid.MyQuickAction;
import com.zwh.app.widget.greendroid.QuickActionGrid;
import com.zwh.app.widget.greendroid.QuickActionWidget;
import com.zwh.app.widget.greendroid.QuickActionWidget.OnQuickActionClickListener;
import com.zwh.app.widget.lazy.ImageLoader;

public class MainActivity extends BaseActivity {
	
	

	private static final String TAG = "MainActivity";
	
	/*
     * ÉêÇëAPP KEYµÄ¾ßÌå½éÉÜ£¬¿É²Î¼û 
     * http://wiki.open.t.qq.com/index.php/Ó¦ÓÃ½ÓÈëÖ¸Òý
     * http://wiki.open.t.qq.com/index.php/ÌÚÑ¶Î¢²©ÒÆ¶¯Ó¦ÓÃ½ÓÈë¹æ·¶#.E6.8E.A5.E5.85.A5.E6.B5.81.E7.A8.8B
     */
    //!!!Çë¸ù¾ÝÄúµÄÊµ¼ÊÇé¿öÐÞ¸Ä!!!      ÈÏÖ¤³É¹¦ºóä¯ÀÀÆ÷»á±»ÖØ¶¨Ïòµ½Õâ¸öurlÖÐ  ±ØÐëÓë×¢²áÊ±ÌîÐ´µÄÒ»ÖÂ
    private String redirectUri="http://www.tencent.com/zh-cn/index.shtml";                   
    //!!!Çë¸ù¾ÝÄúµÄÊµ¼ÊÇé¿öÐÞ¸Ä!!!      »»ÎªÄúÎª×Ô¼ºµÄÓ¦ÓÃÉêÇëµ½µÄAPP KEY
    private String clientId = "要自己申请"; 
    //!!!Çë¸ù¾ÝÄúµÄÊµ¼ÊÇé¿öÐÞ¸Ä!!!      »»ÎªÄúÎª×Ô¼ºµÄÓ¦ÓÃÉêÇëµ½µÄAPP SECRET
    private String clientSecret="要自己申请";
    
    private OAuthV2 oAuth;
	
	public OAuthV2 getoAuth() {
		return oAuth;
	}

	public static final int QUICKACTION_LOGIN_OR_LOGOUT = 0;
	public static final int QUICKACTION_USERINFO = 1;
	public static final int QUICKACTION_SOFTWARE = 2;
	public static final int QUICKACTION_SEARCH = 3;
	public static final int QUICKACTION_SETTING = 4;
	public static final int QUICKACTION_EXIT = 5;
	
	private QuickActionWidget mGrid;// ¿ì½ÝÀ¸¿Ø¼þ
	
	private InputMethodManager imm;
	
	//head ²¿·Ö
	private ImageView mHeadLogo;
	private TextView mHeadTitle;
	private ProgressBar mHeadProgress;
	private ImageButton mHeadEdit;
	private ImageButton mHeadSearch;

    //foot²¿·Ö
	private RadioButton fbHomeLine;
	private RadioButton fbAboutMe;
	private RadioButton fbLetter;
	private RadioButton fbSearch;
	private ImageView fbSetting;
	
	//scrollLayout²¿·Ö
	private ScrollLayout mScrollLayout;
	private RadioButton[] mButtons;
	private String[] mHeadTitles;
	private int mViewCount;
	private int mCurSel;
	
	//listview Ö÷Ò³
	private PullToRefreshListView lvHomeLine;
	private View lvHomeLine_footer;
	private TextView lvHomeLine_foot_more;
	private ProgressBar lvHomeLine_foot_progress;
	private ListViewHomeLineAdapter lvHomeLineAdapter;
	
	//listview ¹ØÓÚÎÒ
	
	private PullToRefreshListView lvMentionsTimeLine;
	private View lvMentionsTimeLine_footer;
	private TextView lvMentionsTimeLine_foot_more;
	private ProgressBar lvMentionsTimeLine_foot_progress;
	private ListViewMentionsTimelineAdapter lvMentionsTimeLineAdapter;
	
	//listview Ë½ÐÅ
	
	private PullToRefreshListView lvPriveteTimeLine;
	private View lvPriveteTimeLine_footer;
	private TextView lvPriveteTimeLine_foot_more;
	private ProgressBar lvPriveteTimeLine_foot_progress;
	private ListViewPrivateAdapter lvPriveteTimeLineAdapter;
	
	private AppContext appContext;// È«¾ÖContext
	
	private ImageLoader imageLoader=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		appContext = (AppContext) getApplication();
		imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		imageLoader=new ImageLoader(this.getApplicationContext());
		AppContext haoDroid = ((AppContext)getApplicationContext());
 		haoDroid.setImageLoader(imageLoader);
 		
		oAuth=new OAuthV2(redirectUri);
        oAuth.setClientId(clientId);
        oAuth.setClientSecret(clientSecret);
		/**
		 * ³õÊ¼»¯¹¤×÷
		 */
		initHeadView();
		initFootBar();
		initPageScroll();
		initFrameListView();
		initQuickActionGrid();

	}
	@Override
	protected void onResume() {
		super.onResume();
		if (mCurSel != 3){
			//Òþ²ØÈí¼üÅÌ WindowManager.LayoutParams.token.
			imm.hideSoftInputFromWindow(null, 0);
			
		}
		
		// ¶ÁÈ¡×óÓÒ»¬¶¯ÅäÖÃ
		mScrollLayout.setIsScroll(appContext.isScroll());
//		UIHelper.Exit(MainActivity.this);
//		AppContext haoDroid = ((AppContext)getApplicationContext());
		if(!appContext.isLogin()){
			// µÇÂ¼
			login(MainActivity.this,MainActivity.this);
		}
//		login(MainActivity.this,MainActivity.this);
		
	}
	
	/**
	 * ³õÊ¼»¯Í·²¿ÊÓÍ¼
	 */
	private void initHeadView() {
		mHeadLogo = (ImageView) findViewById(R.id.main_head_logo);
		mHeadTitle = (TextView) findViewById(R.id.main_head_title);
		mHeadProgress = (ProgressBar) findViewById(R.id.main_head_progress);
		mHeadEdit = (ImageButton) findViewById(R.id.main_head_edit);
		mHeadSearch=(ImageButton) findViewById(R.id.main_head_search);
		mHeadEdit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				UIHelper.showEdit(MainActivity.this);
				
			}
		});
	
	
	}

	/**
	 * ³õÊ¼»¯µ×²¿À¸
	 */
	private void initFootBar() {
		fbHomeLine = (RadioButton) findViewById(R.id.main_footbar_homeline);
		fbAboutMe = (RadioButton) findViewById(R.id.main_footbar_aboutme);
		fbLetter = (RadioButton) findViewById(R.id.main_footbar_letter);
		fbSearch = (RadioButton) findViewById(R.id.main_footbar_search);

		fbSetting = (ImageView) findViewById(R.id.main_footbar_setting);
		fbSetting.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Õ¹Ê¾¿ì½ÝÀ¸&ÅÐ¶ÏÊÇ·ñµÇÂ¼&ÊÇ·ñ¼ÓÔØÎÄÕÂÍ¼Æ¬
				/*UIHelper.showSettingLoginOrLogout(Main.this,
						mGrid.getQuickAction(0));*/
				mGrid.show(v);
			}
		});
	}
	/**
	 * ³õÊ¼»¯Ë®Æ½¹ö¶¯·­Ò³
	 */
	private void initPageScroll() {
		mScrollLayout = (ScrollLayout) findViewById(R.id.main_scrolllayout);
		mScrollLayout.setIsScroll(true);
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_linearlayout_footer);
		mHeadTitles = getResources().getStringArray(R.array.head_titles);
		mViewCount = mScrollLayout.getChildCount();
		mButtons = new RadioButton[mViewCount];

		for (int i = 0; i < mViewCount; i++) {
			mButtons[i] = (RadioButton) linearLayout.getChildAt(i * 2);
			mButtons[i].setTag(i);
			mButtons[i].setChecked(false);
			mButtons[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					int pos = (Integer) (v.getTag());
					
					//Òþ²ØÈí¼üÅÌ WindowManager.LayoutParams.token.
					
					EditText mContent = (EditText)findViewById(R.id.search_editer);
					imm.hideSoftInputFromWindow(mContent.getWindowToken(), 0);
					
					// µã»÷µ±Ç°ÏîË¢ÐÂ
					setCurPoint(pos);
					mScrollLayout.snapToScreen(pos);
				
				}
			});
		}
		// ÉèÖÃµÚÒ»ÏÔÊ¾ÆÁ
		mCurSel = 0;
		mButtons[mCurSel].setChecked(true);
		
		mScrollLayout.SetOnViewChangeListener(new ScrollLayout.OnViewChangeListener() {
			public void OnViewChange(int viewIndex) {
				
				setCurPoint(viewIndex);
				
			}
		});
		
	}
	/**
	 * ÉèÖÃµ×²¿À¸µ±Ç°½¹µã
	 * 
	 * @param index
	 */
	private void setCurPoint(int index) {
		if (index < 0 || index > mViewCount - 1 || mCurSel == index)
			return;

		mButtons[mCurSel].setChecked(false);
		mButtons[index].setChecked(true);
		mHeadTitle.setText(mHeadTitles[index]);
		mCurSel = index;
		
		mHeadEdit.setVisibility(View.GONE);
		mHeadSearch.setVisibility(View.GONE);
		// Í·²¿logo¡¢°´Å¥ÏÔÊ¾
		if (index == 0) {
			mHeadEdit.setVisibility(View.VISIBLE);
			mHeadLogo.setImageResource(R.drawable.frame_logo_homeline);
		} else if (index == 1) {
			mHeadEdit.setVisibility(View.VISIBLE);
			mHeadLogo.setImageResource(R.drawable.frame_logo_aboutme);
		} else if (index == 2) {
			mHeadEdit.setVisibility(View.VISIBLE);
			mHeadLogo.setImageResource(R.drawable.frame_logo_letter);
		} else if (index == 3) {
			mHeadSearch.setVisibility(View.VISIBLE);
			mHeadLogo.setImageResource(R.drawable.frame_logo_search);
			
		}		
		
		
	}
	/**
	 * ³õÊ¼»¯ËùÓÐListView
	 */
	private void initFrameListView() {
		// ³õÊ¼»¯listview¿Ø¼þ   Ö÷Ò³
		this.initHomeLineListView();
		
		// ³õÊ¼»¯listview¿Ø¼þ      ¹ØÓÚÎÒ
		this.initMentionsTimeLineListView();
		
		//³õÊ¼»¯listview¿Ø¼þ    Ë½ÐÅ
		this.initPrivateLineListView();
		
		
	}
	private void initPrivateLineListView(){
		
		lvPriveteTimeLine_footer = getLayoutInflater().inflate(R.layout.listview_footer,
				null);
		lvPriveteTimeLine_foot_more = (TextView) lvPriveteTimeLine_footer
				.findViewById(R.id.listview_foot_more);
		lvPriveteTimeLine_foot_progress = (ProgressBar) lvPriveteTimeLine_footer
				.findViewById(R.id.listview_foot_progress);
		lvPriveteTimeLine=(PullToRefreshListView) findViewById(R.id.frame_listview_letter);
		lvPriveteTimeLine.addFooterView(lvPriveteTimeLine_footer);// Ìí¼Óµ×²¿ÊÓÍ¼ ±ØÐëÔÚsetAdapterÇ°
		
		ArrayList<Map<String,Object>> items = new ArrayList<Map<String,Object>>();
		
		lvPriveteTimeLineAdapter=new ListViewPrivateAdapter(this,items);
		lvPriveteTimeLine.setAdapter(lvPriveteTimeLineAdapter);
		lvPriveteTimeLine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.i("position",Integer.toString(position));
				// µã»÷Í·²¿¡¢µ×²¿À¸ÎÞÐ§
				if (position == 0 || view == lvPriveteTimeLine_foot_more)
					return;
			}
		});
		lvPriveteTimeLine.setOnScrollListener(new AbsListView.OnScrollListener() {
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				lvPriveteTimeLine.onScrollStateChanged(view, scrollState);

				// Êý¾ÝÎª¿Õ--²»ÓÃ¼ÌÐøÏÂÃæ´úÂëÁË
				if (lvPriveteTimeLineAdapter.isEmpty())
					return;

				// ÅÐ¶ÏÊÇ·ñ¹ö¶¯µ½µ×²¿
				boolean scrollEnd = false;
				try {
					if (view.getPositionForView(lvPriveteTimeLine_footer) == view
							.getLastVisiblePosition())
						scrollEnd = true;
				} catch (Exception e) {
					scrollEnd = false;
				}

//				int lvDataState = StringUtils.toInt(lvHomeLine.getTag());
				if (scrollEnd &&scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
//					lvBlog.setTag(UIHelper.LISTVIEW_DATA_LOADING);
					lvPriveteTimeLine_foot_more.setText(R.string.load_ing);
					lvPriveteTimeLine_foot_progress.setVisibility(View.VISIBLE);
					Log.i("isend", "--------end------");
					
					loadListViewPrivateHomeLineData(lvPriveteTimeLineAdapter ,"1");
					
				}
			}

			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				lvPriveteTimeLine.onScroll(view, firstVisibleItem, visibleItemCount,
						totalItemCount);
			}
		});
		lvPriveteTimeLine.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
			public void onRefresh() {
				lvPriveteTimeLine.onRefreshComplete();
				Toast.makeText(getApplicationContext(), "¼ÓÔØÍê³É", Toast.LENGTH_SHORT).show();
			}
		});
		
	}
	//¹ØÓÚÎÒ 
	private void initMentionsTimeLineListView(){
	
		lvMentionsTimeLine_footer = getLayoutInflater().inflate(R.layout.listview_footer,
				null);
		lvMentionsTimeLine_foot_more = (TextView) lvMentionsTimeLine_footer
				.findViewById(R.id.listview_foot_more);
		lvMentionsTimeLine_foot_progress = (ProgressBar) lvMentionsTimeLine_footer
				.findViewById(R.id.listview_foot_progress);
		lvMentionsTimeLine=(PullToRefreshListView) findViewById(R.id.frame_listview_aboutme);
		lvMentionsTimeLine.addFooterView(lvMentionsTimeLine_footer);// Ìí¼Óµ×²¿ÊÓÍ¼ ±ØÐëÔÚsetAdapterÇ°
		
		ArrayList<Map<String,Object>> items = new ArrayList<Map<String,Object>>();
		
		lvMentionsTimeLineAdapter=new ListViewMentionsTimelineAdapter(this,items);
		lvMentionsTimeLine.setAdapter(lvMentionsTimeLineAdapter);
		lvMentionsTimeLine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
//				Log.i("position",Integer.toString(position) );
				// µã»÷Í·²¿¡¢µ×²¿À¸ÎÞÐ§
				if (position == 0 || view == lvMentionsTimeLine_foot_more)
					return;
				

			}
		});
		lvMentionsTimeLine.setOnScrollListener(new AbsListView.OnScrollListener() {
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				lvMentionsTimeLine.onScrollStateChanged(view, scrollState);

				// Êý¾ÝÎª¿Õ--²»ÓÃ¼ÌÐøÏÂÃæ´úÂëÁË
				if (lvMentionsTimeLineAdapter.isEmpty())
					return;

				// ÅÐ¶ÏÊÇ·ñ¹ö¶¯µ½µ×²¿
				boolean scrollEnd = false;
				try {
					if (view.getPositionForView(lvMentionsTimeLine_footer) == view
							.getLastVisiblePosition())
						scrollEnd = true;
				} catch (Exception e) {
					scrollEnd = false;
				}

//				int lvDataState = StringUtils.toInt(lvHomeLine.getTag());
				if (scrollEnd &&scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
//					lvBlog.setTag(UIHelper.LISTVIEW_DATA_LOADING);
					lvMentionsTimeLine_foot_more.setText(R.string.load_ing);
					lvMentionsTimeLine_foot_progress.setVisibility(View.VISIBLE);
					Log.i("isend", "--------end------");
					
					loadListViewMentionsTimeLineData(lvMentionsTimeLineAdapter ,"1");
					/*loadLvBlogData(curNewsCatalog, pageIndex, lvBlogHandler,
							UIHelper.LISTVIEW_ACTION_SCROLL);*/
				}
			}

			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				lvMentionsTimeLine.onScroll(view, firstVisibleItem, visibleItemCount,
						totalItemCount);
			}
		});
		lvMentionsTimeLine.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
			public void onRefresh() {
				/*loadLvBlogData(curNewsCatalog, 0, lvBlogHandler,
						UIHelper.LISTVIEW_ACTION_REFRESH);*/
				
				lvMentionsTimeLine.onRefreshComplete();
				
				Toast.makeText(getApplicationContext(), "¼ÓÔØÍê³É", Toast.LENGTH_SHORT).show();
			}
		});
		
	}
	private void initHomeLineListView(){
	
		
		lvHomeLine_footer = getLayoutInflater().inflate(R.layout.listview_footer,
				null);
		lvHomeLine_foot_more = (TextView) lvHomeLine_footer
				.findViewById(R.id.listview_foot_more);
		lvHomeLine_foot_progress = (ProgressBar) lvHomeLine_footer
				.findViewById(R.id.listview_foot_progress);
		lvHomeLine=(PullToRefreshListView) findViewById(R.id.frame_listview_homeline);
		lvHomeLine.addFooterView(lvHomeLine_footer);// Ìí¼Óµ×²¿ÊÓÍ¼ ±ØÐëÔÚsetAdapterÇ°
		
		ArrayList<Map<String,Object>> items = new ArrayList<Map<String,Object>>();
		
		lvHomeLineAdapter=new ListViewHomeLineAdapter(this,items);
		lvHomeLine.setAdapter(lvHomeLineAdapter);
		lvHomeLine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.i("position",Integer.toString(position) );
				// µã»÷Í·²¿¡¢µ×²¿À¸ÎÞÐ§
				if (position == 0 || view == lvHomeLine_foot_more)
					return;
				

		
			}
		});
		lvHomeLine.setOnScrollListener(new AbsListView.OnScrollListener() {
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				lvHomeLine.onScrollStateChanged(view, scrollState);

				// Êý¾ÝÎª¿Õ--²»ÓÃ¼ÌÐøÏÂÃæ´úÂëÁË
				if (lvHomeLineAdapter.isEmpty())
					return;

				// ÅÐ¶ÏÊÇ·ñ¹ö¶¯µ½µ×²¿
				boolean scrollEnd = false;
				try {
					if (view.getPositionForView(lvHomeLine_footer) == view
							.getLastVisiblePosition())
						scrollEnd = true;
				} catch (Exception e) {
					scrollEnd = false;
				}

//				int lvDataState = StringUtils.toInt(lvHomeLine.getTag());
				if (scrollEnd &&scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
//					lvBlog.setTag(UIHelper.LISTVIEW_DATA_LOADING);
					lvHomeLine_foot_more.setText(R.string.load_ing);
					lvHomeLine_foot_progress.setVisibility(View.VISIBLE);
					Log.i("isend", "--------end------");
					
					loadListViewHomeLineData(lvHomeLineAdapter ,"1");
					
				}
			}

			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				lvHomeLine.onScroll(view, firstVisibleItem, visibleItemCount,
						totalItemCount);
			}
		});
		lvHomeLine.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
			public void onRefresh() {
				//¸üÐÂÊý¾Ý
				loadListViewHomeLineData(lvHomeLineAdapter,"2");
				
				lvHomeLine.onRefreshComplete();
				lvHomeLineAdapter.notifyDataSetChanged();
				Toast.makeText(getApplicationContext(), "¼ÓÔØÍê³É", Toast.LENGTH_SHORT).show();
			}
		});
		
	}
	
	/**
	 * ³õÊ¼»¯¿ì½ÝÀ¸
	 */
	private void initQuickActionGrid() {
		mGrid = new QuickActionGrid(this);
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_login,
				R.string.main_menu_login));
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_myinfo,
				R.string.main_menu_myinfo));
		mGrid.addQuickAction(new MyQuickAction(this,
				R.drawable.ic_menu_software, R.string.main_menu_aboutme));
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_search,
				R.string.main_menu_search));
		mGrid.addQuickAction(new MyQuickAction(this,
				R.drawable.ic_menu_setting, R.string.main_menu_setting));
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_exit,
				R.string.main_menu_exit));

		mGrid.setOnQuickActionClickListener(mActionListener);
	}

	/**
	 * ¿ì½ÝÀ¸itemµã»÷ÊÂ¼þ
	 */
	private OnQuickActionClickListener mActionListener = new OnQuickActionClickListener() {

		public void onQuickActionClicked(QuickActionWidget widget, int position){
			switch (position) {
			case QUICKACTION_LOGIN_OR_LOGOUT:// ÓÃ»§µÇÂ¼-×¢Ïú
				  //¹Ø±ÕOAuthV2ClientÖÐµÄÄ¬ÈÏ¿ªÆôµÄQHttpClient¡£
		        OAuthV2Client.getQHttpClient().shutdownConnection();
				showLogin();
				break;
			case QUICKACTION_USERINFO:// ÎÒµÄ×ÊÁÏ
				
				UIHelper.showUserInfo(MainActivity.this);
				
				break;
			case QUICKACTION_SOFTWARE:// ¹ØÓÚÎÒÃÇ
				UIHelper.showAbout(MainActivity.this);
				break;
			case QUICKACTION_SEARCH:// ËÑË÷
				// Ìø×ªµ½ËÑË÷
				mScrollLayout.snapToScreen(3);
				setCurPoint(3);
				break;
			case QUICKACTION_SETTING:// ÉèÖÃ
				UIHelper.showSetting(MainActivity.this);
				break;
			case QUICKACTION_EXIT:// ÍË³ö
				// ÊÇ·ñÍË³öÓ¦ÓÃ
				UIHelper.Exit(MainActivity.this);
				break;
			}
		}
	};
	
	
	
	/**
	 * ¼ÓÔØÖ÷Ò³Êý¾Ý
	 */
	private void loadListViewHomeLineData(ListViewHomeLineAdapter lvHomeLineAdapter ,String pageflag) {
		
		
		StatusesAPI statusesAPI=new StatusesAPI(OAuthConstants.OAUTH_VERSION_2_A);
        
        try {
//            response=userAPI.info(oAuthV2, "json");//µ÷ÓÃQWeiboSDK»ñÈ¡ÓÃ»§ÐÅÏ¢
        	OAuthV2 oAuthV2=null;
        	AppContext haoDroid = ((AppContext)getApplicationContext());
        	oAuthV2 =(OAuthV2) haoDroid.getOauth();
        	String pagetime=null;
        	//µ÷ÓÃQWeiboSDK»ñÈ¡ÐÅÏ¢
        	if(pageflag.equals("0")){//µÚÒ»Ò³
        		pagetime="0";
        		lvHomeLineAdapter.removeAllItem();
        	}else if(pageflag.equals("1")){//ÏòÏÂ·­Ò³
        		
        		pagetime=lvHomeLineAdapter.getLastPagetime();
        	}else if(pageflag.equals("2")){//ÏòÉÏ·­Ò³
        		pagetime=lvHomeLineAdapter.getFirstPagetime();
        	}
            String json=statusesAPI.homeTimeline(oAuthV2, "json",pageflag,pagetime,"5","0","0");
            
            String hasnext=StatusPaser.getStatuses(json,lvHomeLineAdapter,pageflag);
            
            lvHomeLineAdapter.notifyDataSetChanged();
//            Log.i("number",number);
            if(hasnext.equals("1")){
            	lvHomeLine_foot_more.setText("¼ÓÔØÍê±Ï");
            	lvHomeLine_foot_progress.setVisibility(View.INVISIBLE);
            	
            	return ;
            }
         
        } catch (Exception e) {
            e.printStackTrace();
        }
//        userAPI.shutdownConnection();
        statusesAPI.shutdownConnection();
		
		
	}
	/**
	 * ¼ÓÔØÌáµ½ÎÒµÄÊý¾Ý
	 */
	private void loadListViewMentionsTimeLineData(ListViewMentionsTimelineAdapter lvMentionsTimeLineAdapter ,String pageflag) {
		
		
		StatusesAPI statusesAPI=new StatusesAPI(OAuthConstants.OAUTH_VERSION_2_A);
        
        try {
//            response=userAPI.info(oAuthV2, "json");//µ÷ÓÃQWeiboSDK»ñÈ¡ÓÃ»§ÐÅÏ¢
        	OAuthV2 oAuthV2=null;
        	AppContext haoDroid = ((AppContext)getApplicationContext());
        	oAuthV2 =(OAuthV2) haoDroid.getOauth();
        	String pagetime=null;
        	//µ÷ÓÃQWeiboSDK»ñÈ¡ÐÅÏ¢
        	if(pageflag.equals("0")){//µÚÒ»Ò³
        		pagetime="0";
        		lvMentionsTimeLineAdapter.removeAllItem();
        	}else if(pageflag.equals("1")){//ÏòÏÂ·­Ò³
        		
        		pagetime=lvMentionsTimeLineAdapter.getLastPagetime();
        	}else if(pageflag.equals("2")){//ÏòÉÏ·­Ò³
        		pagetime=null;
        	}
            String json=statusesAPI.mentionsTimeline(oAuthV2, "json",pageflag,pagetime,"5","0","0","0");
     
            String hasnext=MentionPaser.getStatuses(json,lvMentionsTimeLineAdapter);
            if(hasnext.equals("1")){
            	lvMentionsTimeLine_foot_more.setText("¼ÓÔØÍê±Ï");
            	lvMentionsTimeLine_foot_progress.setVisibility(View.INVISIBLE);
            	
            	return ;
            }
            
            
            lvMentionsTimeLineAdapter.notifyDataSetChanged();
//            Log.i("number",number);
            
         
        } catch (Exception e) {
            e.printStackTrace();
        }
//        userAPI.shutdownConnection();
        statusesAPI.shutdownConnection();
		
	}
	/**
	 * ¼ÓÔØË½ÐÅÊý¾Ý
	 */
	private void loadListViewPrivateHomeLineData(ListViewPrivateAdapter lvHomeLineAdapter ,String pageflag) {
		
		
		PrivateAPI statusesAPI=new PrivateAPI(OAuthConstants.OAUTH_VERSION_2_A);
        
        try {
//            response=userAPI.info(oAuthV2, "json");//µ÷ÓÃQWeiboSDK»ñÈ¡ÓÃ»§ÐÅÏ¢
        	OAuthV2 oAuthV2=null;
        	AppContext haoDroid = ((AppContext)getApplicationContext());
        	oAuthV2 =(OAuthV2) haoDroid.getOauth();
        	String pagetime=null;
        	String id="0";
        	//µ÷ÓÃQWeiboSDK»ñÈ¡ÐÅÏ¢
        	if(pageflag.equals("0")){//µÚÒ»Ò³
        		pagetime="0";
        		lvHomeLineAdapter.removeAllItem();
        	}else if(pageflag.equals("1")){//ÏòÏÂ·­Ò³
        		
        		pagetime=lvHomeLineAdapter.getLastPagetime();
        		id=lvHomeLineAdapter.getLastID();
        	}else if(pageflag.equals("2")){//ÏòÉÏ·­Ò³
        		pagetime=null;
        	}
            String json=statusesAPI.recv(oAuthV2, "json",pageflag,pagetime,"5",id,"0");
     
            String hasnext=PrivatePaser.getStatuses(json,lvHomeLineAdapter);
            if(hasnext.equals("1")){
            	lvPriveteTimeLine_foot_more.setText("¼ÓÔØÍê±Ï");
            	lvPriveteTimeLine_foot_progress.setVisibility(View.INVISIBLE);
            	
            	return ;
            }
            lvHomeLineAdapter.notifyDataSetChanged();
//            Log.i("number",number);
            
         
        } catch (Exception e) {
            e.printStackTrace();
        }
//        userAPI.shutdownConnection();
        statusesAPI.shutdownConnection();
		
		
	}
	/**
	 * ´´½¨menu TODO Í£ÓÃÔ­Éú²Ëµ¥
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
		
	}
	/**
	 * ¼àÌý·µ»Ø--ÊÇ·ñÍË³ö³ÌÐò
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean flag = true;
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// ÊÇ·ñÍË³öÓ¦ÓÃ
			UIHelper.Exit(this);
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {
			// Õ¹Ê¾¿ì½ÝÀ¸&ÅÐ¶ÏÊÇ·ñµÇÂ¼
			
			mGrid.show(fbSetting, true);
		}else{
			flag = super.onKeyDown(keyCode, event);
		}
		return flag;
	}
	 /*
     * Í¨¹ý¶ÁÈ¡OAuthV2AuthorizeWebView·µ»ØµÄIntent£¬»ñÈ¡ÓÃ»§ÊÚÈ¨ÐÅÏ¢
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode==2) {
            if (resultCode==OAuthV2AuthorizeWebView.RESULT_CODE){
                oAuth=(OAuthV2) data.getExtras().getSerializable("oauth");
                if(oAuth.getStatus()==0){
                	
                	AppContext haoDroid = ((AppContext)getApplicationContext());
             		haoDroid.setOAuth(oAuth);
                	haoDroid.setLogin(true);
                    Toast.makeText(getApplicationContext(), "µÇÂ½³É¹¦", Toast.LENGTH_SHORT).show();
                   
            		
            		
            		loadListViewHomeLineData(lvHomeLineAdapter,"0");
            		loadListViewMentionsTimeLineData(lvMentionsTimeLineAdapter,"0");
            		loadListViewPrivateHomeLineData(lvPriveteTimeLineAdapter ,"0");
                }
            }
        }
    }
    
    /**
	 * ÏÔÊ¾µÇÂ¼½çÃæ
	 * 
	 * @param context
	 */
	public  void showLogin() {
		
		Intent intent = new Intent(MainActivity.this, LoginActivity.class);//´´½¨Intent£¬Ê¹ÓÃWebViewÈÃÓÃ»§ÊÚÈ¨
        intent.putExtra("oauth", oAuth);
        startActivityForResult(intent,2);
        
	}
	/**
	 * µÇÂ¼³ÌÐò
	 * 
	 * @param cont
	 */
	public static void login(final Context cont,final MainActivity activity) {
		AlertDialog.Builder builder = new AlertDialog.Builder(cont);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setTitle(R.string.app_login);
		builder.setPositiveButton(R.string.sure,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						activity.showLogin();
					}
				});
		builder.setNegativeButton(R.string.cancle,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// ÍË³ö
						AppManager.getAppManager().AppExit(cont);
					}
				});
		builder.show();
	}
	
}
