package com.zwh.app.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zwh.android_hao.R;
import com.zwh.app.common.StringUtils;
import com.zwh.app.manager.AppContext;
import com.zwh.app.widget.lazy.ImageLoader;

public class ListViewPrivateAdapter extends BaseAdapter {
    
    private Activity activity;
//    private String[] data;
    //分页标示
    private String pageflag;
    //起始时间
    private String pagetime="0";
    private String id="0";
    
	private List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public ListViewPrivateAdapter(Activity a,List<Map<String,Object>> list) {
    	
        activity = a;
        this.list=list;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        AppContext haoDroid = ((AppContext)activity.getApplicationContext());
        imageLoader=haoDroid.getImageLoader();
    }
    @Override
    public int getCount() {
    	return this.list.size();
    }
    @Override
    public Object getItem(int position) {
    	return list.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null){
            vi = inflater.inflate(R.layout.homeline_listitem, null);
        }
        TextView text_nick=(TextView)vi.findViewById(R.id.homeline_listitem_username);
        TextView text_origtext=(TextView)vi.findViewById(R.id.homeline_listitem_content);
        TextView text_timestamp=(TextView)vi.findViewById(R.id.homeline_listitem_date);
        ImageView image=(ImageView)vi.findViewById(R.id.homeline_listitem_userface);
        String username=this.list.get(position).get("nick").toString();
        String content=this.list.get(position).get("origtext").toString();
        String timestamp=this.list.get(position).get("timestamp").toString();
        
        timestamp=StringUtils.converTime(Long.parseLong(timestamp));
        text_nick.setText(username);
        text_origtext.setText(content);
        text_timestamp.setText(timestamp);
        String url=this.list.get(position).get("head").toString();
        url=url+"/50";
        imageLoader.DisplayImage(url, image);
        
        return vi;
    }
    /**
	 * 添加列表项
	 * @param item
	 */
	public void addItem(Map<String,Object> item) {
		
		list.add(item);
		
	}
	/**
	 * 清空列表项
	 * @param item
	 */
	public void removeAllItem() {
		list.clear();
		
		
	}
	public String getFirstPagetime() {
		if(!list.isEmpty()){
			pagetime=this.list.get(0).get("timestamp").toString();
		}
		
		return pagetime;
	}
	public String getLastPagetime() {
		if(!list.isEmpty()){
			int number=list.size();
			pagetime=this.list.get(number-1).get("timestamp").toString();
		}
		
		return pagetime;
	}
	public String getFirstID() {
		if(!list.isEmpty()){
			id=this.list.get(0).get("id").toString();
		}
		
		return id;
	}
	public String getLastID() {
		if(!list.isEmpty()){
			int number=list.size();
			id=this.list.get(number-1).get("id").toString();
		}
		
		return id;
	}
	
}